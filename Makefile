OBJECTS = hello-makefile.o
TARGET = hello-makefile

all: $(TARGET)

$(TARGET): $(OBJECTS)
		$(CC) $(CFLAGS) $(LDFLAGS) $(OBJECTS) -o $(TARGET)
%.o : %.c
	$(CC) $(CFLAGS) -c $<
clean: 
	$(RM) $(OBJECTS) $(TARGET) 

install: $(TARGET)
	install -m 0755 $(TARGET) $(DESTDIR)

.PHONY: clean install
	

